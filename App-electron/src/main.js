const {app, BrowserWindow, Menu, ipcMain} = require('electron')
const url = require('url')
const path = require('path')
const io  = require('socket.io-client')('http://localhost:3000/')

/**aplication variables */
let mainWindow;
let newImpressionWindow; 


if(process.env.NODE_ENV !== 'production'){
    require('electron-reload')(__dirname, {
        electron : path.join(__dirname, '../node_modules', '.bin', 'electron')
    })
}

app.on('ready', ()=> {
    mainWindow =  new BrowserWindow({
    });
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname,'views/index.html'),
        protocol: 'file',
        slashes: true
    }))
    const mainMenu = Menu.buildFromTemplate(templateMenu)
    Menu.setApplicationMenu(mainMenu)
    mainWindow.on('closed', () => {
        app.quit()
    })


    io.on('connect', () => {
        console.log("runnig")

        io.on('chat:message', data=> {
            console.log('running');
            console.log(data)
        })
    })
    
})



const newImpression = () => {
    newImpressionWindow = new BrowserWindow({
        width: 400,
        height: 500,
        title: "Impresión en proceso",
        webPreferences: {
            nodeIntegration: true 
        }
    })
    //newImpressionWindow.setMenu(null);
    newImpressionWindow.loadURL(url.format({
        pathname: path.join(__dirname,  './views/impresion.html'),
        protocol: 'file',
        slashes: true
    }))
    newImpressionWindow.on('closed', () => {
        newImpressionWindow = null 
    })
    
}


ipcMain.on('cerrar:evento', (e, impresion) => {
    console.log(impresion)

    newImpressionWindow.close();
})




const templateMenu = [
    {
        label: 'App',
        submenu: [
            {
                label: 'aplicacion',
                click(){
                    newImpression();
                }
            },
            {
                label: 'Salir',
                accelerator: process.platform == 'darwin'? 'command + Q' : 'Ctrl + Q',
                click(){
                    app.quit();
                }
            }
        ]
    }
]

if (process.env.NODE_ENV !== 'production') {
    templateMenu.push(
        {
            label: 'DevTools',
            submenu: [
                {
                    label: 'Show/Hide devTools',
                    accelerator: 'Ctrl + D',
                    click(item, focusedWindow){
                        focusedWindow.toggleDevTools();
                    }
                },
                {
                    role: 'reload'
                }
            ]
        },
        )
}


/**
 * Server configuration
 * 
 */
