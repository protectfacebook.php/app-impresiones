const express  = require('express'); 
const { urlencoded } = require('express');
const app = express();
const routes = require('./routes/route')
const path = require('path')
const  multer  =   require('multer');
const  storage =   multer.diskStorage({
    destination: function (req, file, callback) {
      callback(null, path.join(__dirname, '../files'));
    },
    filename: function (req, file, callback) {
      callback(null, file.originalname );
    }
  });
const upload = multer({storage});

app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(require('./routes/route'))
app.set('port', process.env.PORT || 3000)
app.use(express.static(path.join(__dirname, '../views')))




let server  = app.listen(app.get('port'), ()=> {
    console.log("app running in port",  app.get('port'))
   
})
const io = require('socket.io')(server);
io.on('connection', socket  => {
    console.log('conecction refused')
    socket.on('chat:message', (data)=> {
        io.sockets.emit('chat:message', data)
    })
    socket.on('uploaded:file', (data) => {
        console.log('funcionando correctamente ')
    })
})

app.post('/invoice/api', upload.single('archivo'), (req, res, err)=> {
        res.send('<h1>subido<h1>')
        io.emit('uploaded:file', req)
        
})







