const socket = io()
let message = document.getElementById('message')
let userName = document.getElementById('userName')
let btn  = document.getElementById('send')
let output = document.getElementById('output')
let actions = document.getElementById('actions')

btn.addEventListener('click', () => {
    socket.emit('chat:message',{
        message: message.value,
        userName: userName.value
    } )
})


socket.on('chat:message', (data)=> {
    output.innerHTML += `<p><strong>${data.userName}</strong>:
    ${data.message}
    </p>`
})
